Source: kubrick
Section: games
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Daniel Schepler <schepler@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Modestas Vainius <modax@debian.org>,
           George Kiagiadakis <gkiagiad@csd.uoc.gr>,
           Eshat Cakar <info@eshat.de>,
           Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>,
           Maximiliano Curia <maxy@debian.org>,
Build-Depends: cmake (>= 2.8.12~),
               debhelper-compat (= 12),
               kf6-extra-cmake-modules,
               libgl1-mesa-dev,
               libglu1-mesa-dev,
               kf6-kcolorscheme-dev,
               kf6-kconfig-dev,
               kf6-kconfigwidgets-dev,
               kf6-kcoreaddons-dev,
               kf6-kcrash-dev,
               kf6-kdoctools-dev,
               kf6-ki18n-dev,
               libkdegames6-dev,
               kf6-kio-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kxmlgui-dev,
               libqt6opengl6-dev,
               qt6-svg-dev,
               pkg-kde-tools-neon,
               qt6-base-dev,
Standards-Version: 4.1.4
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kubrick
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kubrick.git

Package: kubrick
Architecture: amd64
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: khelpcenter
Description: game based on Rubik's Cube
 Kubrick is a game based on Rubik's Cube and using OpenGL 3-D graphics
 libraries.
 .
 Kubrick handles cubes, "bricks" and "mats" from 2x2x1 up to 6x6x6. It has
 several built-in puzzles of graded difficulty, as well as demos of solving
 moves and pretty patterns. The game has unlimited undo, redo, save and
 reload capabilities.
 .
 This package is part of the KDE games module.
